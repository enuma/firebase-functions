// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database. 
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

function validateUser(idToken, res) {
  return admin.auth().verifyIdToken(idToken)
  .then(function(decodedToken) {
    let email = decodedToken.email.toLowerCase();
    if (!email.endsWith('robustastudio.com')) {
      throw new Error('Invalid email!', decodedToken);
    }
    let uid = decodedToken.uid;
    return uid;
  }).catch(err => {
    res.sendStatus(403);
    console.error('Error Validating User:', err);
  });
}

function setOrigin(req, res) {
  let allowedDomains = [
    'https://robusta-secrets-management.firebaseapp.com',
    'http://localhost:3000',
    'https://secrets.robustastudio.com',
  ];
  let incomingOrigin = req.headers.origin;
  let allowedDomain = allowedDomains.includes(incomingOrigin)? incomingOrigin: allowedDomains[0];
  res.set('Access-Control-Allow-Origin', allowedDomain)
  res.set('Access-Control-Allow-Methods', 'GET, POST')
}

exports.listProjects = functions.https.onRequest((req, res) => {
  setOrigin(req, res);
  validateUser(req.query.idToken, res).then(uid => {
    admin.database().ref('/projects_names').once('value', snapshot => {
      console.log('Fteched Projects List', snapshot.exportVal());
      res.send(snapshot.exportVal());
    }).catch(err => {
      console.error('Error getting list of projects', err);
      res.sendStatus(500);
    });
  });
});

exports.getProject = functions.https.onRequest((req, res) => {
  setOrigin(req, res);
  validateUser(req.query.idToken, res).then(uid => {
    admin.database().ref(`/projects/${req.query.name}`).once('value', snapshot => {
      console.log('Fteched Project Details', snapshot.exportVal());
      res.send(snapshot.exportVal());
    }).catch(err => {
      console.error('Error getting project details =>', req.query.name, err);
      res.sendStatus(500);
    });
  });
});
